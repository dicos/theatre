{% load i18n %}
Новое сообщение с сайта

Имя: {{ name }}
Как связаться: {{ contact }}
-------
Сообщение: {{ body }}
-------
{% if meta %}

META:
{% for key, value in meta.items %} {{ key }}: {{ value }}
{% endfor %}

{% endif %}
