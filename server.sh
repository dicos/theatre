#!/bin/bash

cd /home/teatr/theatre_/theatre
case "$1" in
    "start")
        sudo -u teatr ../bin/uwsgi --ini uwsgi.ini
        ;;
    "stop")
        kill -9 `cat /tmp/theatre.pid`
        ;;
    "restart")
        ./server.sh stop
        ./server.sh start
        ;;
    *) 
        echo "Usage: ./server.sh {start|stop|restart}"
        ;;
esac
