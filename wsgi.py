"""
WSGI config for theatre project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/howto/deployment/wsgi/
"""

import os, sys
#os.environ.setdefault("DJANGO_SETTINGS_MODULE", "theatre.settings")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")
#os.environ.setdefault('ROOT_URLCONF', 'theatre.urls.urlpatterns')

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
