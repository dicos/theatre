#coding: utf8
from django.contrib import admin


class BaseAdmin(admin.ModelAdmin):
    ''' Disable delete actions '''
    def has_delete_permission(self, request, obj=None):
        ''' Hide delete button into page for edites info of django admin site '''
        return False

    def get_actions(self, request):
        ''' Delete action "Delete select posts" into list posts of
            django admin site '''
        actions = super(BaseAdmin, self).get_actions(request)
        if 'delete_selected' in actions:
            del actions['delete_selected']
        return actions

