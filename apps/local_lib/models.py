#coding: utf8
from django.core.cache import cache
from django.db.models import Manager, Model, BooleanField

from .managers import BaseManager

class BaseModel(Model):
    class Meta:
        abstract = True

    def __unicode__(self):
        return self.__str__()

    def __str__(self):
        raise NotImplementedError

    is_active = BooleanField("активный", default=True)

    objects = BaseManager()
    all_objects = Manager()

    def delete(self, *args, **kwargs):
        self.is_active = False
        self.save()

    def save(self, *args, **kwargs):
        super(BaseModel, self).save(*args, **kwargs)
        cache.clear()
