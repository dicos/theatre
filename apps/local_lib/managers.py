#coding: utf8
from django.db import models
from django.db.models.query import QuerySet


class BaseQuerySet(QuerySet):
    def delete(self):
        self.update(is_active=False)

    def delete_real(self):
        super(BaseQuerySet, self).delete()

    def active(self):
        return self.filter(is_active=True)


class BaseManager(models.Manager):
    def get_query_set(self):
        return BaseQuerySet(self.model, using=self._db).active()

    def active(self):
        return self.filter(is_active=True)
