#coding: utf8
from django.contrib import admin

from local_lib.admin import BaseAdmin

from .models import Category, Post, Staff


class CategoryAdmin(BaseAdmin):
    prepopulated_fields = {'slug': ('name',)}
    list_display = ('name', 'slug', 'is_active',)


class PostAdmin(BaseAdmin):
    list_display = ('name', 'is_active',)


class StaffAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('full_name',)}
    list_display = ('full_name', 'slug', 'photo', 'phone', 'post',)


admin.site.register(Category, CategoryAdmin)
admin.site.register(Post, PostAdmin)
admin.site.register(Staff, StaffAdmin)
