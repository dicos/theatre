#coding: utf8
from django.db import models

from cms.models import CMSPlugin
from djangocms_text_ckeditor.fields import HTMLField

from local_lib.models import BaseModel


class Category(BaseModel):
    """ Category of posts """
    name = models.CharField('Категория должности', max_length=50, unique=True)
    position = models.SmallIntegerField('Позиция в меню',
                                        default=100,
                                        db_index=True)
    slug = models.SlugField(db_index=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ('position', 'name',)
        verbose_name = 'Категория должности'
        verbose_name_plural = 'Категории должностей'


class Post(BaseModel):
    """ Posts of staff """
    category = models.ForeignKey(Category, verbose_name='Категория должности')
    name = models.CharField('Должность', max_length=50, unique=True)
    order = models.SmallIntegerField("Как будет выводиться",
                                     default=100,
                                     db_index=True,
                                     help_text='Чем меньше цифра, тем более в переди будет стоять')

    def __str__(self):
        return '%s (%s)' % (self.name, self.category.name)

    class Meta:
        ordering = ('order', 'name', 'category__name',)
        verbose_name = 'Должность'
        verbose_name_plural = 'Должности'


class Staff(BaseModel):
    ''' List of staff '''
    full_name = models.CharField('ФИО', max_length=150, db_index=True)
    slug = models.SlugField(db_index=True)
    photo = models.ImageField(upload_to="staff", verbose_name='Фотография')
    birthday = models.DateField(verbose_name='Дата рождения')
    about = HTMLField(verbose_name='Биография', blank=True)
    phone = models.CharField('Телефон', max_length=30, blank=True)
    post = models.ForeignKey(Post, verbose_name='Должность')

    def __str__(self):
        return self.full_name

    @property
    def first_name(self):
        name_params = self.full_name.split(' ', 2)
        return name_params[0] if name_params else ''

    @property
    def last_name(self):
        name_params = self.full_name.split(' ', 3)
        return name_params[1] if len(name_params) >= 2 else ''

    @property
    def patronymic(self):
        name_params = self.full_name.split(' ', 3)
        return name_params[1] if len(name_params) == 3 else ''

    class Meta:
        ordering = ('full_name',)
        verbose_name = 'Персона'
        verbose_name_plural = 'Персоналии'
