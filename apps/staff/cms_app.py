#coding; utf8
from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool

from .menu import StaffMenu


class StaffApp(CMSApp):
    name = 'Категории должностей'
    urls = ['staff.urls']
    app_name = 'staff_category'
    menus = [StaffMenu]


apphook_pool.register(StaffApp)
