#coding: utf8
from django.shortcuts import render, get_object_or_404, redirect
from django.views.decorators.cache import cache_page

from news.models import News
from repertory.models import Person
from .models import Staff, Category


@cache_page(60 * 60 * 24)
def category_index(request):
    category = Category.objects.all()[0].slug
    return redirect('staff_category', permanent=True, category_slug=category)


@cache_page(60 * 60 * 24)
def category(request, category_slug):
    category = get_object_or_404(Category, slug=category_slug)
    persons = Staff.objects.filter(post__category=category) \
                            .order_by('post__order', 'full_name')
    data = {'persons': persons,
            'category': category}
    return render(request, 'staff/category.html', data)


@cache_page(60 * 60 * 24)
def info(request, category_slug, name_slug):
    person = get_object_or_404(Staff,
                               slug=name_slug,
                               post__category__slug=category_slug)
    related_news = person.staffnews_set.values('news_id')
    news = News.objects.active().filter(pk__in=related_news)[:6]
    roles = person.person_set.all().select_related('performance')
    data = {'person': person,
            'news_list': news,
            'roles': roles}
    return render(request, 'staff/info.html', data)
