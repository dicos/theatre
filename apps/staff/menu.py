#coding: utf8
from django.core.urlresolvers import reverse

from cms.menu_bases import CMSAttachMenu
from menus.base import Menu, NavigationNode
from menus.menu_pool import menu_pool

from .models import Category, Staff


def get_staff_id(category_slug, staff_slug):
    """ get unique staff id """
    return category_slug + staff_slug


class StaffMenu(CMSAttachMenu):
    """ Menu for create breadgrumbs and menu staff pages """
    name = 'Меню персоналий'

    def get_nodes(self, request):
        nodes = []
        categories = set([])
        staff_qs = Staff.objects.filter(is_active=True) \
                                .select_related('position', 'post__category')
        for staff in staff_qs:
            category = staff.post.category
            categories.add((category.name, category.slug, category.position,))
            args = (category.slug, staff.slug)
            node = NavigationNode(staff.full_name,
                                  reverse('staff_info', args=args),
                                  get_staff_id(category.slug, staff.slug),
                                  parent_id=category.slug)
            nodes.append(node)
        for name, slug, *args in sorted(categories, key=lambda x: (x[2], x[0],)):
            node = NavigationNode(name,
                                  reverse('staff_category', args=(slug,)),
                                  slug)
            nodes.append(node)
        return nodes


menu_pool.register_menu(StaffMenu)
