from django.conf.urls import patterns, include, url


urlpatterns = patterns('staff.views',
    url(r'^$', 'category_index', name='staff_category_index'),
    url(r'^(?P<category_slug>.+)/$', 'category', name='staff_category'),
    url(r'^(?P<category_slug>.+)/(?P<name_slug>.+)\.html$', 'info', name='staff_info'),
)
