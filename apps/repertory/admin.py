#coding: utf8
from django.contrib import admin

from local_lib.admin import BaseAdmin

from .models import Genre, Performance, Photo, Person


class GenreAdmin(BaseAdmin):
    list_display = ('name', 'is_active',)


class PhotoInline(admin.TabularInline):
    model = Photo
    readonly_fields = ('image_tag',)


class PersonInline(admin.TabularInline):
    model = Person


class PerformanceAdmin(BaseAdmin):
    list_display = ('name', 'is_premiere', 'genre',
                    'duration', 'get_age_limit_display',)
    prepopulated_fields = {'slug': ('name',)}
    inlines = (PersonInline, PhotoInline,)


admin.site.register(Genre, GenreAdmin)
admin.site.register(Performance, PerformanceAdmin)
