from django.conf.urls import patterns, include, url


urlpatterns = patterns('repertory.views',
    url(r'^$', 'catalog', name='repertory_catalog'),
    url(r'^(?P<performance_slug>.+)\.html$', 'info', name='repertory_info'),
)
