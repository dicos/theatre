#coding: utf8
from django.shortcuts import render
from django.http import Http404
from django.views.decorators.cache import cache_page

from news.models import News
from .models import Performance, WorkEnum


@cache_page(60 * 60 * 24)
def catalog(request):
    ''' Show active performances '''
    qs = Performance.objects.active()
    data = {'performances': qs}
    return render(request, 'repertory/catalog.html', data)


@cache_page(60 * 60 * 24)
def info(request, performance_slug):
    """ Show performance info """
    try:
        performance = Performance.objects.select_related('genre') \
                                         .get(slug=performance_slug)
    except Performance.DoesNotExists:
        raise Http404
    persons_qs = performance.person_set.active() \
                                       .select_related('staff__post__category') \
                                       .order_by('-work', 'role_name')
    actors = []
    staffs = []
    for person in persons_qs:
        if person.work == WorkEnum.ACTOR:
            actors.append(person)
        elif person.work == WorkEnum.OTHER:
            staffs.append(person)
    related_news = performance.performancenews_set.values('news_id')
    news = News.objects.active().filter(pk__in=related_news)[:6]
    data = {'performance': performance,
            'photos': performance.photo_set.active(),
            'actors': actors,
            'staffs': staffs,
            'news_list': news}
    return render(request, 'repertory/info.html', data)
