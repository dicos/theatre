#coding; utf8
from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool

from .menu import RepertoryMenu


class RepertoryApp(CMSApp):
    name = 'Репертуар'
    urls = ['repertory.urls']
    app_name = 'repertory'
    menus = [RepertoryMenu]


apphook_pool.register(RepertoryApp)
