#coding: utf8
from datetime import date

from django.db import models
from django.core.exceptions import ValidationError
from djangocms_text_ckeditor.fields import HTMLField
from django_enumfield import enum

from local_lib.models import BaseModel
from staff.models import Staff


class Genre(BaseModel):
    ''' Genres for performances '''
    name = models.CharField('Жанр', max_length=100, unique=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Жанр'
        verbose_name_plural = 'Жанры'
        ordering = ('name',)


class AgeLimitEnum(enum.Enum):
    ''' enums for age limit for performances '''
    ZERO = 0
    SIX = 1
    TVELVE = 2
    SIXTEEN = 3
    EIGHTEEN = 4

    labels = {ZERO: '0+',
              SIX: '6+',
              TVELVE: '12+',
              SIXTEEN: '16+',
              EIGHTEEN: '18+'}


class Performance(BaseModel):
    ''' All performances into this theatre '''
    name = models.CharField('Название', max_length=150)
    slug = models.SlugField(db_index=True, unique=True)
    is_premiere = models.BooleanField('премьера?', default=False)
    genre = models.ForeignKey(Genre, verbose_name='Жанр')
    author = models.CharField('Драматург', max_length=150)
    date_premiere = models.DateField(verbose_name='Дата премьеры',
                                     default=date.today)
    duration = models.TimeField('Продолжительность')
    about = HTMLField('Описание')
    age_limit = enum.EnumField(AgeLimitEnum,
                               verbose_name='Возрастное ограничение',
                               default=AgeLimitEnum.ZERO)
    preview = models.ImageField(upload_to="performance",
                                verbose_name='главное фото спектакля')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Спектакль'
        verbose_name_plural = 'Спектакли'
        ordering = ('name',)


class Photo(BaseModel):
    ''' photos from performance '''
    performance = models.ForeignKey(Performance)
    photo = models.ImageField(upload_to="performance/photo")

    def __str__(self):
        return 'Фотография для %s' % self.performance

    def image_tag(self):
        return u'<img src="%s" />' % self.photo.url
    image_tag.short_description = 'Фотография'
    image_tag.allow_tags = True


class WorkEnum(enum.Enum):
    ACTOR = 1
    OTHER = 2

    labels = {ACTOR: 'Актерский состав',
             OTHER: 'Прочие'}


class Person(BaseModel):
    """ Persons into performance """
    role_name = models.CharField('название роли', max_length=150)
    performance = models.ForeignKey(Performance, verbose_name='Спектакль')
    staff = models.ForeignKey(Staff, blank=True, null=True,
                              verbose_name=u'Персона')
    fio = models.CharField('ФИО', blank=True, null=True,
                           max_length=150,
                           help_text='заполнить, если нет в списке персон!')
    work = enum.EnumField(WorkEnum,
                          verbose_name='Участие в спектакле',
                          db_index=True)

    def clean(self):
        if not (self.fio or self.staff):
            raise ValidationError('Нужно заполнить поле "Персона" или "ФИО"')

    def __str__(self):
        params = (self.performance,
                  self.staff or self.fio,
                  self.get_work_display())
        return '%s -- %s -- %s' % params

    class Meta:
        verbose_name = 'Участник в спектакле'
        verbose_name_plural = 'Участники в спектаклях'
        ordering = ('work', 'performance__name', 'role_name',
                    'staff__full_name', 'fio',)
