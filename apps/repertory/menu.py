#coding: utf8
from django.core.urlresolvers import reverse

from cms.menu_bases import CMSAttachMenu
from menus.base import Menu, NavigationNode
from menus.menu_pool import menu_pool

from .models import Performance


class RepertoryMenu(CMSAttachMenu):
    """ Menu for create breadgrumbs and menu staff pages """
    name = 'Меню репертуара'

    def get_nodes(self, request):
        nodes = []
        performance_qs = Performance.objects.active()
        for performance in performance_qs:
            args = (performance.slug,)
            node = NavigationNode(performance.name,
                                  reverse('repertory_info', args=args),
                                  performance.slug)
            nodes.append(node)
        return nodes


menu_pool.register_menu(RepertoryMenu)
