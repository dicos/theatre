#coding: utf8
from cms.models.pagemodel import Page
from django.contrib import admin

from local_lib.admin import BaseAdmin

from .models import ExtendedPage


class ExtendedPageAdmin(BaseAdmin):
    list_display = ('page', 'date_changed',)


admin.site.register(ExtendedPage, ExtendedPageAdmin)
