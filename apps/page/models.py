#coding: utf8
from django.db import models
from django.utils.translation import ugettext_lazy as _
from cms.models import Title
from djangocms_text_ckeditor.fields import HTMLField

from local_lib.models import BaseModel


class ExtendedPage(BaseModel):
    page = models.OneToOneField(Title,
                                verbose_name=_("Page"))
    content = HTMLField(verbose_name='Содержание')
    date_changed = models.DateTimeField("Дата изменения",
                                        auto_now_add=True)
    
    def __str__(self):
        return self.page.title

    class Meta:
        verbose_name = 'Текст для страницы'
        verbose_name_plural = 'Текст для страниц'
