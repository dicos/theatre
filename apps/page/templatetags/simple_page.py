#coding: utf8
from django.db.models import Q
from django import template

from page.models import ExtendedPage

register = template.Library()


@register.simple_tag
def page_content(page):
    cnd = Q(page__page__pk=page.pk) | Q(page__publisher_public__pk=page.pk)
    try:
        content = ExtendedPage.objects.get(cnd)
    except ExtendedPage.DoesNotExist:
        return ''
    return content.content
