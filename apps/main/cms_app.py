#coding; utf8
from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool


class MainApp(CMSApp):
    name = 'Главная страница'
    app_name = 'main'
    urls = ['main.urls']


apphook_pool.register(MainApp)
