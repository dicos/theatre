#coding: utf8
from datetime import datetime, date

from django.conf import settings
from django.shortcuts import render
from django.views.decorators.cache import cache_page
from pytils.dt import ru_strftime

from news.models import News
from repertory.models import Performance
from partner.models import Partner
from playbill.models import Playbill
from playbill.utils import get_separated_playbills


@cache_page(60 * 60 * 24)
def main(request):
    qs = Playbill.objects.active() \
                         .filter(date__gt=date.today()) \
                         .select_related('performance__genre')
    latest = qs[:settings.LATEST_COUNT]
    for_photos = Performance.objects.active() \
                                    .order_by("?")[:settings.LATEST_COUNT]
    news = News.objects.active()[:settings.LATEST_COUNT]
    data = {'latest': latest,
            'playbills': get_separated_playbills(qs),
            'photos': Performance.objects,
            'for_photos': for_photos,
            'news_list': news,
            'partners': Partner.objects.active()}
    return render(request, 'main/main.html', data)
