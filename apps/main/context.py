#coding: utf8
from django.conf import settings

from repertory.models import Performance


def for_photos(request):
    ''' return dict for block 'right-sidebar' '''
    for_photos = Performance.objects.active() \
                                    .order_by("?")[:settings.LATEST_COUNT]
    return {'for_photos': for_photos}
