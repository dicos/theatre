from django.conf.urls import patterns, include, url


urlpatterns = patterns('playbill.views',
    url(r'^$', 'playbill', name='playbill')
)
