#coding: utf8
from django.db import models

from local_lib.models import BaseModel

from repertory.models import Performance


class Playbill(BaseModel):
    ''' Date and time present performance '''
    performance = models.ForeignKey(Performance, verbose_name='Спектакль')
    date = models.DateTimeField(verbose_name='Дата спектакля', db_index=True)

    def __str__(self):
        params = (self.performance, self.date.strftime("%d %B %Y"))
        return "%s - %s" % params

    class Meta:
        ordering = ('date',)
        verbose_name = 'Спектакль'
        verbose_name_plural = 'Афиша'
