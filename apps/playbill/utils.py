#coding: utf8
from pytils.dt import ru_strftime


def get_separated_playbills(playbill_qs):
    """ returning separated playbills by month """
    playbills = []
    playbills_month = []
    prev_month = None
    prev_year = None
    for playbill in playbill_qs:
        curr_month = ru_strftime("%B", date=playbill.date)
        if not prev_month:
            prev_month = curr_month
        elif prev_month != curr_month:
            playbills.append((prev_month, prev_year, playbills_month))
            prev_month = curr_month
            playbills_month = []
        playbills_month.append(playbill)
        prev_year = playbill.date.strftime("%Y")
    if prev_year:
        playbills.append((curr_month, prev_year, playbills_month))
    return playbills
