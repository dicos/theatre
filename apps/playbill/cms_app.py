#coding; utf8
from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool


class PlaybillApp(CMSApp):
    name = 'Афиша'
    urls = ['playbill.urls']
    app_name = 'playbill'


apphook_pool.register(PlaybillApp)
