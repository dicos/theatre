#coding: utf8
from datetime import datetime, date

from django.shortcuts import render
from django.views.decorators.cache import cache_page

from .models import Playbill
from .utils import get_separated_playbills


@cache_page(60 * 60 * 24)
def playbill(request):
    qs = Playbill.objects.active() \
                         .filter(date__gt=date.today()) \
                         .select_related('performance__genre')
    data = {'playbills': get_separated_playbills(qs)}
    return render(request, 'playbill/playbill.html', data)
