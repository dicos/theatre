#coding: utf8
from django.contrib import admin

from local_lib.admin import BaseAdmin

from .models import Playbill


class PlaybillAdmin(BaseAdmin):
    list_display = ('performance', 'date',)


admin.site.register(Playbill, PlaybillAdmin)
