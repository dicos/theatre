#coding; utf8
from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool



class FeedbackApp(CMSApp):
    name = 'Обратная связь'
    urls = ['feedback_form.urls']
    app_name = 'feedback_form'


apphook_pool.register(FeedbackApp)
