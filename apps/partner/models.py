#coding: utf8
from django.db import models

from local_lib.models import BaseModel


class Partner(BaseModel):
    ''' Partner '''
    url = models.URLField('Сайт')
    logo = models.ImageField(upload_to='partner', verbose_name='логотип')
    short_description = models.CharField('Краткое описание', max_length=200)
    position = models.SmallIntegerField('Позиция в меню',
                                        default=100,
                                        db_index=True)

    def logo_tag(self):
        return u'<img src="%s" width="100" />' % self.logo.url
    logo_tag.short_description = 'Логотип'
    logo_tag.allow_tags = True

    def __str__(self):
        return self.short_description

    class Meta:
        verbose_name = 'Партнер'
        verbose_name_plural = 'Партнеры'
        ordering = ('position', 'pk')

