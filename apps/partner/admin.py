#coding: utf8
from django.contrib import admin

from local_lib.admin import BaseAdmin

from .models import Partner


class PartnerAdmin(BaseAdmin):
    list_display = ('short_description', 'logo_tag', 'url', 'position',)


admin.site.register(Partner, PartnerAdmin)

