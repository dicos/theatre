#coding: utf8
from django.core.urlresolvers import reverse

from cms.menu_bases import CMSAttachMenu
from menus.base import Menu, NavigationNode
from menus.menu_pool import menu_pool

from .models import News


class NewsMenu(CMSAttachMenu):
    """ Menu for create breadgrumbs and menu news pages """
    name = 'Меню новостей'

    def get_nodes(self, request):
        nodes = []
        for news in News.objects.active():
            args = (news.pk,)
            node = NavigationNode(news.name,
                                  reverse('news_info', args=args),
                                  news.pk)
            nodes.append(node)
        return nodes


menu_pool.register_menu(NewsMenu)
