from django.conf.urls import patterns, include, url
from django.views.decorators.cache import cache_page

from .views import NewsList


urlpatterns = patterns('news.views',
    url(r'^$', cache_page(60 * 60 * 24)(NewsList.as_view()), name='news_list'),
    url(r'^(?P<news_id>\d+)\.html$', 'info', name='news_info'),
)
