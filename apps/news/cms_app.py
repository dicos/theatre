#coding; utf8
from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool

from .menu import NewsMenu


class NewsApp(CMSApp):
    name = 'Новости'
    urls = ['news.urls']
    app_name = 'news'
    menus = [NewsMenu]


apphook_pool.register(NewsApp)
