#coding: utf8
from django.contrib import admin

from local_lib.admin import BaseAdmin

from .models import News, StaffNews, PerformanceNews, Photo


class StaffNewsInline(admin.TabularInline):
    model = StaffNews


class PerformanceNewsInline(admin.TabularInline):
    model = PerformanceNews


class PhotoInline(admin.TabularInline):
    model = Photo
    readonly_fields = ('image_tag',)


class NewsAdmin(BaseAdmin):
    list_display = ('date_created', 'name',)
    inlines = (PhotoInline, StaffNewsInline, PerformanceNewsInline,)


admin.site.register(News, NewsAdmin)
