#coding: utf8
from datetime import datetime

from django.db import models
from djangocms_text_ckeditor.fields import HTMLField

from local_lib.models import BaseModel
from repertory.models import Performance
from staff.models import Staff


class News(BaseModel):
    ''' News '''
    date_created = models.DateTimeField('Дата новости', default=datetime.now)
    name = models.CharField('Заголовок', max_length=200)
    short_content = models.TextField('Краткое описание')
    content = HTMLField('Сама новость')
    preview = models.ImageField(upload_to="news",
                                verbose_name='фото новости',
                                blank=True, null=True)

    def __str__(self):
        return '%s %s' % (self.name, self.date_created.strftime('%d.%m.%Y'))

    class Meta:
        verbose_name = 'Новость'
        verbose_name_plural = 'Новости'
        ordering = ('-date_created',)


class Photo(BaseModel):
    ''' photos from news '''
    news = models.ForeignKey(News)
    photo = models.ImageField(upload_to="news/photo")

    def __str__(self):
        return 'Фотография для %s' % self.news

    def image_tag(self):
        return u'<img src="%s" />' % self.photo.url
    image_tag.short_description = 'Фотография'
    image_tag.allow_tags = True


class StaffNews(BaseModel):
    """ In it news has info about current person """
    news = models.ForeignKey(News)
    staff = models.ForeignKey(Staff, verbose_name='О ком написана новость?')

    def __str__(self):
        return "%s %s" % (self.news, self.staff)

    class Meta:
        verbose_name = 'О ком написана новость?'


class PerformanceNews(BaseModel):
    ''' In it news has info about performance '''
    news = models.ForeignKey(News)
    performance = models.ForeignKey(Performance,
                                    verbose_name='Для какого спектакля?')

    def __str__(self):
        return "%s %s" % (self.news, self.performance)

    class Meta:
        verbose_name = 'Для какого спектакля?'
