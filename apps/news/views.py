#coding; utf8
from django.http import Http404
from django.shortcuts import render, get_object_or_404
from django.views.generic import ListView
from django.views.decorators.cache import cache_page

from local_lib.paginator import QuerySetDiggPaginator

from .models import News


class NewsList(ListView):
    queryset = News.objects.active()
    template_name = 'news/list.html'
    paginate_by = 10
    paginator_class = QuerySetDiggPaginator


@cache_page(60 * 60 * 24)
def info(request, news_id):
    """ Show performance info """
    news = get_object_or_404(News, pk=news_id)
    data = {'news': news,
            'photos': news.photo_set.all()}
    return render(request, 'news/info.html', data)
